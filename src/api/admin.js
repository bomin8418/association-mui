import fetch from '@/config/fetch'

/**
 * 获取所有
 */
export const getAdminList = () => fetch('http://localhost:8080/association/administrators/all');

/**
 * 获取一个
 */
export const getAdmin = data => fetch('http://localhost:8080/association/administrators/getOne', data);
/**
 * 添加
 */
export const addAdmin = data => fetch('http://localhost:8080/association/administrators/insert', data, 'POST');
/**
 * 删除
 */
export const deleteAdmin = data => fetch('http://localhost:8080/association/administrators/delete', data);
/**
 * 修改
 */
export const updateAdmin = data => fetch('http://localhost:8080/association/administrators/update', data, 'POST');
