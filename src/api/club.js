import fetch from '@/config/fetch'

/**
 * 社团列表
 */
export const getClublist = () => fetch('http://localhost:8080/association/club/getClubs','GET');
