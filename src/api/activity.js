import fetch from '@/config/fetch'

/**
 * 社团名称
 */
export const getClublist = () => fetch('http://localhost:8080/association/club/getClubs','GET');
/**
 * 申请活动
 */
export const addActivity = data => fetch('http://localhost:8080/association/activity/insert', data, 'POST');
/**
 * 拒绝活动
 */
export const rejectActivity = data => fetch('http://localhost:8080/association/activity/rejectActivity', data);

/**
 * 同意活动
 */
export const agreesActivity = data => fetch('http://localhost:8080/association/activity/agreesActivity', data);
/**
 * 获取审核人的活动
 *
 */
export const getTeacherActivity = () => fetch('http://localhost:8080/association/activity/getTeacherActivity');
/**
 * 取消活动
 */
export const deleteActivity = data => fetch('http://localhost:8080/association/activity/deleteActivity', data);
/**
 * 获取申请人活动列表
 */
export const getActivityLists = () => fetch('http://localhost:8080/association/activity/all', 'GET');
