import fetch from '@/config/fetch'
/**
 * 学生登陆
 */

export const login = data => fetch('http://localhost:8080/association/login', data, 'POST');


/**
 * 老师登录
 */
export const loginT = data => fetch('http://localhost:8080/association/loginT', data, 'POST');
/**
 * 退出
 */

export const signout = () => fetch('/admin/signout');
