// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios' ;
import Vuex from 'vuex'
import './assets/css/global.css'
import {resetForm} from "@/utils/form";

// 全局方法挂载
Vue.prototype.resetForm = resetForm
Vue.prototype.$axios= axios ;

Vue.config.productionTip = false;
Vue.use(ElementUI,);
Vue.use(Vuex) ;


/* eslint-disable no-new */
new Vue({
  //这里要配置store
  router, store:store,
  render: function (h) { return h(App) }
}).$mount('#app')


