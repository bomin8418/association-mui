import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    meta: {
      title: '登录界面'
    },
    redirect: '/adminLogin'
  },
  //前台
  {
    path: '/index',
    name: 'index',
    meta: {
      title: '主页'
    },
    component: () => import('../views/ui/index.vue'),
    redirect: '/index/home',
    children: [
      {
        path: 'home',
        name: 'home',
        meta: {
          title: '首页'
        },
        component: () => import('../views/ui/Home.vue')
      },
      {
        path: 'clubList',
        name: 'clubList',
        meta: {
          title: '社团列表'
        },
        component: () => import('../views/ui/club/ClubList.vue')
      },
      {
        path: 'activities',
        name: 'activities',
        meta: {
          title: '社团列表'
        },
        component: () => import('../views/ui/activity/Activities.vue')
      },
      {
        path: 'news',
        name: 'news',
        meta: {
          title: '新闻列表'
        },
        component: () => import('../views/ui/news/News.vue')
      },
    ]
  },

  //学生管理员后台
  {
    path: '/adminLogin',
    name: 'adminLogin',
    meta: {
      title: '管理员登录界面'
    },
    component: () => import('../views/manager/AdminLogin.vue')
  },
  {
    path: '/managerHome',
    name: 'ManagerHome',
    meta: {
      title: '主页'
    },
    component: () => import('../views/manager/ManagerHome.vue'),
    redirect: '/managerHome/manageWelcome',
    children: [
      {
        path: 'manageWelcome',//暂时与managerHomeT的welcome重复定义，有警告
        name: 'ManageWelcome',
        meta: {
          title: '学生端后台管理'
        },
        component: () => import('../views/manager/Welcome.vue')
      },
      {
        path: 'addActivity',
        meta: {
          title: '活动申请'
        },
        component: () => import('../views/manager/activitymanage/AddActivity')
      },
      {
        path: 'activityList',
        meta: {
          title: '活动列表'
        },
        component: () => import('../views/manager/activitymanage/ActivityList')
      },
      {
        path: 'addClub',
        meta: {
          title: '新增社团'
        },
        component: () => import('../views/manager/clubmanage/AddClub')
      }
    ]
  },
  //教师管理员后台
  {
    path: '/managerHomeT',
    name: 'ManagerHomeT',
    meta: {
      title: '主页'
    },
    component: () => import('../views/manager/ManagerHomeT.vue'),
    redirect: '/managerHomeT/manageWelcomeT',
    children: [
      {
        path: 'manageWelcomeT',
        name: 'ManageWelcomeT',
        meta: {
          title: '教师端后台管理'
        },
        component: () => import('../views/manager/WelcomeT.vue')
      },
      {
        path: 'activityApprove',
        meta: {
          title: '活动审批'
        },
        component: () => import('../views/manager/activitymanage/ActivityApprove')
      },
      {
        path: 'adminList',
        meta: {
          title: '管理员管理'
        },
        component: () => import('../views/manager/adminmanage/AdminList')
      },
      // {
      //   path: 'allClub',
      //   meta: {
      //     title: '社团管理'
      //   },
      //   component: () => import('../views/manager/clubmanage/ClubList')
      // }
    ]
  }

]

// 路由导航守卫

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

// router.beforeEach((to, from, next) => {
//   //to 即将要访问的路径
//   //from 代表从哪里来
//   //next 表示放行
//
//   if (to.path === '/login' || to.path === 'adminLogin') return next()
//
//   //获取token
//   const tokenStr = window.sessionStorage.getItem('token')
//   if(!tokenStr) return  next('login')
//   next()
// })

export default router
